package com.pattarapol.week10;

public class Triangle extends Shape{
    private double A;
    private double B;
    private double C;

    public Triangle(double A , double B, double C){
        super("Triangle");
        this.A = A;
        this.B = B;
        this.C = C;

    }

    @Override
    public double calArea() {
        double s = (A + B + C) / 2;
        s = s * (s - A) * (s - B) * (s - C);
        s = Math.sqrt(s);
        return s;

    }

    @Override
    public double calPerimeter() {
        return A + B + C;
    }

    @Override
    public String toString() {
        return this.getName() + " A:"+this.A + " B:" + this.B + " C:" + this.C;

    }

    
}
